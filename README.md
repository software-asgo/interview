# Interview

This repo was created with the purpose of provide a transparent interview
process for the fullstack engineer @ ASGO.

## Structure

This repo is structured in branches, each one with different programming
languages, the current fullstack engineer should improvise some questions about
it when they meet at the interview.
